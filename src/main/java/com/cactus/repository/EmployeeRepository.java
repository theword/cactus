package com.cactus.repository;

import org.springframework.data.repository.CrudRepository;

import com.cactus.domain.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long>{
	
}
