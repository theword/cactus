/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cactus.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "title", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Title.findAll", query = "SELECT t FROM Title t"),
    @NamedQuery(name = "Title.findById", query = "SELECT t FROM Title t WHERE t.id = :id"),
    @NamedQuery(name = "Title.findByCode", query = "SELECT t FROM Title t WHERE t.code = :code"),
    @NamedQuery(name = "Title.findByTitleName", query = "SELECT t FROM Title t WHERE t.titleName = :titleName"),
    @NamedQuery(name = "Title.findByCreateon", query = "SELECT t FROM Title t WHERE t.createon = :createon"),
    @NamedQuery(name = "Title.findByStatus", query = "SELECT t FROM Title t WHERE t.status = :status")})
public class Title implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "title_name", nullable = false, length = 2000000000)
    private String titleName;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private double createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @JoinColumn(name = "createby", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createby;
    @OneToMany(mappedBy = "titleId", fetch = FetchType.LAZY)
    private List<User> userList;

    public Title() {
    }

    public Title(Integer id) {
        this.id = id;
    }

    public Title(Integer id, String code, String titleName, double createon, int status) {
        this.id = id;
        this.code = code;
        this.titleName = titleName;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public double getCreateon() {
        return createon;
    }

    public void setCreateon(double createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Title)) {
            return false;
        }
        Title other = (Title) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Title[ id=" + id + " ]";
    }
    
}
