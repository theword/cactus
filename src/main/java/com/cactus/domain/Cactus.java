/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cactus.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "cactus", catalog = "", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"image_uuid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cactus.findAll", query = "SELECT c FROM Cactus c"),
    @NamedQuery(name = "Cactus.findById", query = "SELECT c FROM Cactus c WHERE c.id = :id"),
    @NamedQuery(name = "Cactus.findByCode", query = "SELECT c FROM Cactus c WHERE c.code = :code"),
    @NamedQuery(name = "Cactus.findByCactusName", query = "SELECT c FROM Cactus c WHERE c.cactusName = :cactusName"),
    @NamedQuery(name = "Cactus.findByScienceName", query = "SELECT c FROM Cactus c WHERE c.scienceName = :scienceName"),
    @NamedQuery(name = "Cactus.findByPrice", query = "SELECT c FROM Cactus c WHERE c.price = :price"),
    @NamedQuery(name = "Cactus.findByCreateon", query = "SELECT c FROM Cactus c WHERE c.createon = :createon"),
    @NamedQuery(name = "Cactus.findByStatus", query = "SELECT c FROM Cactus c WHERE c.status = :status"),
    @NamedQuery(name = "Cactus.findByImageUuid", query = "SELECT c FROM Cactus c WHERE c.imageUuid = :imageUuid")})
public class Cactus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "cactus_name", nullable = false, length = 2000000000)
    private String cactusName;
    @Column(name = "science_name", length = 2000000000)
    private String scienceName;
    @Column(name = "message", length = 2000000000)
    private String message;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price", precision = 2000000000, scale = 10)
    private Double price;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private long createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private Integer status;
    @Column(name = "image_uuid", length = 2000000000)
    private String imageUuid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cactusId", fetch = FetchType.LAZY)
    private List<CactusProgress> cactusProgressList;
    @JoinColumn(name = "createby", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createby;

    @OneToMany(mappedBy = "parentId", fetch = FetchType.LAZY)
    private List<Cactus> cactusList;
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cactus parentId;
    
    public Cactus() {
    }

    public Cactus(Integer id) {
        this.id = id;
    }

    public Cactus(Integer id, String code, String cactusName, long createon, int status) {
        this.id = id;
        this.code = code;
        this.cactusName = cactusName;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCactusName() {
        return cactusName;
    }

    public void setCactusName(String cactusName) {
        this.cactusName = cactusName;
    }

    public String getScienceName() {
        return scienceName;
    }

    public void setScienceName(String scienceName) {
        this.scienceName = scienceName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public long getCreateon() {
        return createon;
    }

    public void setCreateon(long createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImageUuid() {
        return imageUuid;
    }

    public void setImageUuid(String imageUuid) {
        this.imageUuid = imageUuid;
    }

    @XmlTransient
    public List<CactusProgress> getCactusProgressList() {
        return cactusProgressList;
    }

    public void setCactusProgressList(List<CactusProgress> cactusProgressList) {
        this.cactusProgressList = cactusProgressList;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }
    
    
    @XmlTransient
    public List<Cactus> getCactusList() {
        return cactusList;
    }

    public void setCactusList(List<Cactus> cactusList) {
        this.cactusList = cactusList;
    }

    public Cactus getParentId() {
        return parentId;
    }

    public void setParentId(Cactus parentId) {
        this.parentId = parentId;
    }
    
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cactus)) {
            return false;
        }
        Cactus other = (Cactus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Cactus[ id=" + id + " ]";
    }
    
}
