/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cactus.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "user", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
    @NamedQuery(name = "User.findByCode", query = "SELECT u FROM User u WHERE u.code = :code"),
    @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByFirstname", query = "SELECT u FROM User u WHERE u.firstname = :firstname"),
    @NamedQuery(name = "User.findByLastname", query = "SELECT u FROM User u WHERE u.lastname = :lastname"),
    @NamedQuery(name = "User.findByShortName", query = "SELECT u FROM User u WHERE u.shortName = :shortName"),
    @NamedQuery(name = "User.findByCreateon", query = "SELECT u FROM User u WHERE u.createon = :createon"),
    @NamedQuery(name = "User.findByStatus", query = "SELECT u FROM User u WHERE u.status = :status")})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "username", nullable = false, length = 2000000000)
    private String username;
    @Basic(optional = false)
    @Column(name = "email", nullable = false, length = 2000000000)
    private String email;
    @Basic(optional = false)
    @Column(name = "password", nullable = false, length = 2000000000)
    private String password;
    @Basic(optional = false)
    @Column(name = "firstname", nullable = false, length = 2000000000)
    private String firstname;
    @Basic(optional = false)
    @Column(name = "lastname", nullable = false, length = 2000000000)
    private String lastname;
    @Basic(optional = false)
    @Column(name = "short_name", nullable = false, length = 2000000000)
    private String shortName;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private double createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby", fetch = FetchType.LAZY)
    private List<CactusProgress> cactusProgressList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby", fetch = FetchType.LAZY)
    private List<ProgressPicture> progressPictureList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby", fetch = FetchType.LAZY)
    private List<AttachFile> attachFileList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby", fetch = FetchType.LAZY)
    private List<Cactus> cactusList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby", fetch = FetchType.LAZY)
    private List<Title> titleList;
    @OneToMany(mappedBy = "createby", fetch = FetchType.LAZY)
    private List<User> userList;
    @JoinColumn(name = "createby", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createby;
    @JoinColumn(name = "title_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Title titleId;
    @JoinColumn(name = "icon", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AttachFile icon;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public User(Integer id, String code, String username, String email, String password, String firstname, String lastname, String shortName, double createon, int status) {
        this.id = id;
        this.code = code;
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.shortName = shortName;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public double getCreateon() {
        return createon;
    }

    public void setCreateon(double createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @XmlTransient
    public List<CactusProgress> getCactusProgressList() {
        return cactusProgressList;
    }

    public void setCactusProgressList(List<CactusProgress> cactusProgressList) {
        this.cactusProgressList = cactusProgressList;
    }

    @XmlTransient
    public List<ProgressPicture> getProgressPictureList() {
        return progressPictureList;
    }

    public void setProgressPictureList(List<ProgressPicture> progressPictureList) {
        this.progressPictureList = progressPictureList;
    }

    @XmlTransient
    public List<AttachFile> getAttachFileList() {
        return attachFileList;
    }

    public void setAttachFileList(List<AttachFile> attachFileList) {
        this.attachFileList = attachFileList;
    }

    @XmlTransient
    public List<Cactus> getCactusList() {
        return cactusList;
    }

    public void setCactusList(List<Cactus> cactusList) {
        this.cactusList = cactusList;
    }

    @XmlTransient
    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }

    public Title getTitleId() {
        return titleId;
    }

    public void setTitleId(Title titleId) {
        this.titleId = titleId;
    }

    public AttachFile getIcon() {
        return icon;
    }

    public void setIcon(AttachFile icon) {
        this.icon = icon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.User[ id=" + id + " ]";
    }
    
}
