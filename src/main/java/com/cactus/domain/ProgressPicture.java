/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cactus.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "progress_picture", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProgressPicture.findAll", query = "SELECT p FROM ProgressPicture p"),
    @NamedQuery(name = "ProgressPicture.findById", query = "SELECT p FROM ProgressPicture p WHERE p.id = :id"),
    @NamedQuery(name = "ProgressPicture.findByCode", query = "SELECT p FROM ProgressPicture p WHERE p.code = :code"),
    @NamedQuery(name = "ProgressPicture.findByCreateon", query = "SELECT p FROM ProgressPicture p WHERE p.createon = :createon"),
    @NamedQuery(name = "ProgressPicture.findByStatus", query = "SELECT p FROM ProgressPicture p WHERE p.status = :status")})
public class ProgressPicture implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "createon", nullable = false)
    private double createon;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @JoinColumn(name = "createby", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createby;
    @JoinColumn(name = "attach_file_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AttachFile attachFileId;
    @JoinColumn(name = "cactus_progress_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CactusProgress cactusProgressId;

    public ProgressPicture() {
    }

    public ProgressPicture(Integer id) {
        this.id = id;
    }

    public ProgressPicture(Integer id, String code, double createon, int status) {
        this.id = id;
        this.code = code;
        this.createon = createon;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getCreateon() {
        return createon;
    }

    public void setCreateon(double createon) {
        this.createon = createon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getCreateby() {
        return createby;
    }

    public void setCreateby(User createby) {
        this.createby = createby;
    }

    public AttachFile getAttachFileId() {
        return attachFileId;
    }

    public void setAttachFileId(AttachFile attachFileId) {
        this.attachFileId = attachFileId;
    }

    public CactusProgress getCactusProgressId() {
        return cactusProgressId;
    }

    public void setCactusProgressId(CactusProgress cactusProgressId) {
        this.cactusProgressId = cactusProgressId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProgressPicture)) {
            return false;
        }
        ProgressPicture other = (ProgressPicture) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProgressPicture[ id=" + id + " ]";
    }
    
}
