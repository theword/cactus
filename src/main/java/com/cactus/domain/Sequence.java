/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cactus.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SimpleBoy
 */
@Entity
@Table(name = "sequence", catalog = "", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"code"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sequence.findAll", query = "SELECT s FROM Sequence s"),
    @NamedQuery(name = "Sequence.findById", query = "SELECT s FROM Sequence s WHERE s.id = :id"),
    @NamedQuery(name = "Sequence.findByCode", query = "SELECT s FROM Sequence s WHERE s.code = :code"),
    @NamedQuery(name = "Sequence.findBySequenceNumber", query = "SELECT s FROM Sequence s WHERE s.sequenceNumber = :sequenceNumber"),
    @NamedQuery(name = "Sequence.findByStatus", query = "SELECT s FROM Sequence s WHERE s.status = :status")})
public class Sequence implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code", nullable = false, length = 2000000000)
    private String code;
    @Basic(optional = false)
    @Column(name = "sequence_number", nullable = false)
    private int sequenceNumber;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;

    public Sequence() {
    }

    public Sequence(Integer id) {
        this.id = id;
    }

    public Sequence(Integer id, String code, int sequenceNumber, int status) {
        this.id = id;
        this.code = code;
        this.sequenceNumber = sequenceNumber;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sequence)) {
            return false;
        }
        Sequence other = (Sequence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Sequence[ id=" + id + " ]";
    }
    
}
