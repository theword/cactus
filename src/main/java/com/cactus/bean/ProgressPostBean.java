package com.cactus.bean;

public class ProgressPostBean extends PostBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3299288060179025205L;

	private String parentId;

	
	public ProgressPostBean() {
		super();
	}

	public ProgressPostBean(String parentId) {
		super();
		this.parentId = parentId;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	
}
