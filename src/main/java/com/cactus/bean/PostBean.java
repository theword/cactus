package com.cactus.bean;

import java.io.Serializable;
import java.util.Date;

public class PostBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6483782799990291412L;
	private String id;
	private String message;
	private String imageUuid;
	private Date createon;
	private Document document;
	private String parentId;
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the imageUuid
	 */
	public String getImageUuid() {
		return imageUuid;
	}
	/**
	 * @param imageUuid the imageUuid to set
	 */
	public void setImageUuid(String imageUuid) {
		this.imageUuid = imageUuid;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the createon
	 */
	public Date getCreateon() {
		return createon;
	}
	/**
	 * @param createon the createon to set
	 */
	public void setCreateon(Date createon) {
		this.createon = createon;
	}
	
	/**
	 * @return the document
	 */
	public Document getDocument() {
		return document;
	}
	/**
	 * @param document the document to set
	 */
	public void setDocument(Document document) {
		this.document = document;
	}
	
	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostBean [id=" + id + ", message=" + message + ", imageUuid="
				+ imageUuid + ", createon=" + createon + ", document="
				+ document + ", parentId=" + parentId + "]";
	}

	
	
}
