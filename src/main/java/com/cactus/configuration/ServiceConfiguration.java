package com.cactus.configuration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cactus.dao.impl.DocumentDao;



@Configuration
@EnableTransactionManagement
@PropertySource(value="application-config.properties")
@ComponentScan( lazyInit=true, basePackages={"com.cactus.repository","com.cactus.service", "com.cactus.dao"})
public class ServiceConfiguration{
	
	private final static Logger log = Logger.getLogger(ServiceConfiguration.class);
	
	@Autowired
	public Environment env;
	
	@Bean(name="documentDao")
	public DocumentDao documentDao(){
		final String filePath = env.getProperty("file.upload.url");
		log.info("Path to save file : " + filePath );
		DocumentDao documentDao = new DocumentDao();
		documentDao.setDirectory( filePath );
		return documentDao;
	}
	
}
