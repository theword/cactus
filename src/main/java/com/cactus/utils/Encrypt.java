package com.cactus.utils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.log4j.Logger;


public class Encrypt implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4300385892187563905L;
	private static final Logger log = Logger.getLogger(Encrypt.class); 
	private static final String UNICODE = "utf-8";
	private static final String SECRET_KEY_FAC_TYPE = "DES";
	private static DESKeySpec keySpec;
	private static SecretKeyFactory keyFactory;
	private static SecretKey key;
	
	static{
		try {
			keySpec = new DESKeySpec( String.valueOf( Encrypt.serialVersionUID).getBytes(Encrypt.UNICODE));
			keyFactory = SecretKeyFactory.getInstance(Encrypt.SECRET_KEY_FAC_TYPE);
			key = keyFactory.generateSecret(Encrypt.keySpec);
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Encode using basic encoder
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String base64Encoder( String str ) throws UnsupportedEncodingException{
		String base64 = Base64.getEncoder().encodeToString(str.getBytes(UNICODE));
		return base64;
	}

	/**
	 * 
	 * @param base64
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String base64Decoder( String base64 ) throws UnsupportedEncodingException{
		byte[] base64decodedBytes = Base64.getDecoder().decode(base64);
		String str = new String(base64decodedBytes, UNICODE);
		log.debug("Original String: " + str);
		return str;
	}



	/**
	 * ENCODE plain text String
	 * @param str
	 * @return
	 */
	public static String encode( String str ) {
		try{
			byte[] cleartext = str.getBytes(UNICODE);      
			Cipher cipher = Cipher.getInstance(Encrypt.SECRET_KEY_FAC_TYPE); // cipher is not thread safe
			cipher.init(Cipher.ENCRYPT_MODE, Encrypt.key);
			String encryptedPwd = Base64.getEncoder().encodeToString(cipher.doFinal(cleartext));
			return encryptedPwd;
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException  | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	


	/**DECODE encrypted String
	 * 
	 * @param encrypted
	 * @return
	 */
	public static String decode( String encrypted ) {

		try {
			byte[] encrypedPwdBytes = Base64.getDecoder().decode(encrypted);
			Cipher cipher = Cipher.getInstance(Encrypt.SECRET_KEY_FAC_TYPE);
			cipher.init(Cipher.DECRYPT_MODE, Encrypt.key);
			byte[] plainTextBytes = (cipher.doFinal(encrypedPwdBytes));
			return new String(plainTextBytes);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// cipher is not thread safe

		return null;
	}
	
	
//	public static void main( String[] args ){
//		String str = "asdadas";
//		
//		System.out.println( encode(str));
//		
//	}
//	public static void main(String[] args) throws Exception {
//	    KeyGenerator kg = KeyGenerator.getInstance("DESede");
//	    Key sharedKey = kg.generateKey();
//
//	    String password = "password";
//	    byte[] salt = "salt1234".getBytes();
//	    PBEParameterSpec paramSpec = new PBEParameterSpec(salt, 20);
//	    PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
//	    SecretKeyFactory kf = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
//	    SecretKey passwordKey = kf.generateSecret(keySpec);
//	    Cipher c = Cipher.getInstance("PBEWithMD5AndDES");
//	    c.init(Cipher.WRAP_MODE, passwordKey, paramSpec);
//	    byte[] wrappedKey = c.wrap(sharedKey);
//
//	    c = Cipher.getInstance("DESede");
//	    c.init(Cipher.ENCRYPT_MODE, sharedKey);
//	    byte[] input = "input".getBytes();
//	    byte[] encrypted = c.doFinal(input);
//	    c = Cipher.getInstance("PBEWithMD5AndDES");
//
//	    c.init(Cipher.UNWRAP_MODE, passwordKey, paramSpec);
//	    Key unwrappedKey = c.unwrap(wrappedKey, "DESede", Cipher.SECRET_KEY);
//
//	    c = Cipher.getInstance("DESede");
//	    c.init(Cipher.DECRYPT_MODE, unwrappedKey);
//	    System.out.println(new String(c.doFinal(encrypted)));
//	  }
}







