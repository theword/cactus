package com.cactus.dao.interfaces;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import com.cactus.domain.Sequence;


@Transactional
public interface ISequenceDao extends CrudRepository<Sequence, Integer>{
	
	public Sequence findBycode(String code);
}
