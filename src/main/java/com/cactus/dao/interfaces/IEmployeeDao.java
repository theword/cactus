package com.cactus.dao.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.cactus.domain.Employee;

public interface IEmployeeDao extends CrudRepository<Employee, Long>{
	
}
