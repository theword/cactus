package com.cactus.dao.interfaces;

import org.springframework.data.repository.CrudRepository;

import com.cactus.domain.User;

public interface IUserDao extends CrudRepository<User, Integer>{

}
