package com.cactus.dao.interfaces;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cactus.domain.Cactus;


public interface ICactusDao extends JpaRepository<Cactus, Integer>{ 
	
	Page<Cactus> findByStatusOrderByCreateonDesc(Integer status, Pageable pageable );
	Page<Cactus> findByIdOrParentIdOrderByCreateonDesc(Integer id, Cactus parentId, Pageable pageable );
	
//	@Query("FROM Cactus c WHERE c.id = {}}")
//	public List<String> findMD5( );
}
