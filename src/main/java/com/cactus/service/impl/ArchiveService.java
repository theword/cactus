package com.cactus.service.impl;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cactus.bean.Document;
import com.cactus.bean.DocumentMetadata;
import com.cactus.dao.interfaces.IDocumentDao;
import com.cactus.service.interfaces.IArchiveService;

/**
 * A service to save, find and get documents from an archive. 
 * 
 * @author Daniel Murygin <daniel.murygin[at]gmail[dot]com>
 */
@Service("archiveService")
public class ArchiveService implements IArchiveService, Serializable {

    private static final long serialVersionUID = 8119784722798361327L;
    
    @Autowired
    private IDocumentDao DocumentDao;

    /**
     * Saves a document in the archive.
     * @see org.murygin.archive.service.IArchiveService#save(org.murygin.archive.service.Document)
     */
    @Override
    public DocumentMetadata save(Document document) {
        getDocumentDao().insert(document); 
        return document.getMetadata();
    }
    
    /**
     * Finds document in the archive
     * @see org.murygin.archive.service.IArchiveService#findDocuments(java.lang.String, java.util.Date)
     */
    @Override
    public List<DocumentMetadata> findDocuments(String personName, Date date) {
        return getDocumentDao().findByPersonNameDate(personName, date);
    }
    
    /**
     * Returns the document file from the archive
     * @see org.murygin.archive.service.IArchiveService#getDocumentFile(java.lang.String)
     */
    @Override
    public byte[] getDocumentFile(String id) {
        Document document = getDocumentDao().load(id);
        if(document!=null) {

        	System.out.println( document.toString());
        	return document.getFileData();
        } else {
            return null;
        }
    }
    
    @Override
    public byte[] getDocumentFileResizing( String id ) throws ApplicationException{
		int width = 128;
		int height = 96;
    	byte[] fileData = getDocumentFile( id );
    	if( fileData != null){
    		return scale(fileData, width, height);
    	}else{
    		return null;
    	}
    }
    
    
    public byte[] scale(byte[] fileData, int width, int height ) throws ApplicationException {
    	
    	try {
    		ByteArrayInputStream in = new ByteArrayInputStream(fileData);
    		BufferedImage img = ImageIO.read(in);
    		if(height == 0) {
    			height = (width * img.getHeight())/ img.getWidth(); 
    		}
    		if(width == 0) {
    			width = (height * img.getWidth())/ img.getHeight();
    		}
    		Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
    		BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    		imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

    		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    		ImageIO.write(imageBuff, "jpg", buffer);

    		return buffer.toByteArray();
    	} catch (IOException e) {
    		throw new ApplicationException("IOException in scale", null);
    	} catch( NullPointerException e ){
    		throw new ApplicationException("Nullpointer in scale", null);
    	}
    }

    public IDocumentDao getDocumentDao() {
        return DocumentDao;
    }

    public void setDocumentDao(IDocumentDao documentDao) {
        DocumentDao = documentDao;
    }


}
