package com.cactus.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.cactus.bean.Document;
import com.cactus.bean.PostBean;
import com.cactus.constants.Constants;
import com.cactus.constants.DateUtil;
import com.cactus.dao.interfaces.ICactusDao;
import com.cactus.dao.interfaces.IDocumentDao;
import com.cactus.dao.interfaces.IUserDao;
import com.cactus.domain.Cactus;
import com.cactus.domain.User;
import com.cactus.service.interfaces.ICactusService;
import com.cactus.service.interfaces.ISequenceService;
import com.cactus.utils.Encrypt;

@Service
@Transactional
public class CactusService implements ICactusService{
	
	@Autowired
	private IDocumentDao documentDao;
	@Autowired
	private Environment env;
	@Autowired
	private ICactusDao cactusDao;
	@Autowired
	private ISequenceService sequenceService;
	@Autowired 
	private IUserDao userDao;
	
	private  static final Logger log = Logger.getLogger(CactusService.class);
	
	
	@Override
	public void getAll(){
		List<Cactus> cactus = (List<Cactus>) cactusDao.findAll();
		for(Cactus c: cactus){
			System.out.println(c.getId());
		}
	}
	
	@Override
	public boolean deletePost( PostBean post ){
		try{
			Integer id = Integer.valueOf(Encrypt.decode(String.valueOf(post.getId())));
			Cactus catus = cactusDao.findOne( id );
			catus.setStatus( Constants.Table.INACTIVE );
			cactusDao.save(catus);
			return true;
		}catch( Exception e){
			return false;
		}
	}
	
	@Override
	public List<PostBean> getPhotoDetailChunk( int pagging, String id ){
		int sizing = Integer.parseInt( env.getProperty("pagging.size") );
		Integer postId = Integer.valueOf(Encrypt.decode(id));
		Cactus cactus = cactusDao.getOne(postId);
		if( cactus.getParentId() != null ){
			cactus = cactus.getParentId();
		}
		Page<Cactus> result = cactusDao.findByIdOrParentIdOrderByCreateonDesc(postId, cactus ,new PageRequest(pagging, sizing ));
		return listToBean( result.getContent() );
	}
	
	@Override
	public List<PostBean> getChunk( int pagging ){
		int sizing = Integer.parseInt( env.getProperty("pagging.size") );
		Page<Cactus> cactus = cactusDao.findByStatusOrderByCreateonDesc( Constants.Table.ACTIVE, new PageRequest(pagging, sizing )  );
		return listToBean( cactus.getContent() );
	}

	@Override
	public PostBean save(PostBean post) {
		User user = userDao.findOne(1);
		log.info( user.getId() + " " + user.getUsername() );
		Cactus cactus = new Cactus();
		cactus.setCode("x");
		cactus.setCreateby(user);
		cactus.setCreateon(  DateUtil.now().getTime() );
		cactus.setStatus( Constants.Table.ACTIVE );
		cactus.setCactusName(post.getMessage());
		cactus.setMessage( post.getMessage() );
		
		if( post.getImageUuid() != null ){
			Document document = documentDao.loadInformation(post.getImageUuid());
			cactus.setImageUuid( post.getImageUuid() );
			if( document != null ){
				post.setDocument(document);
			}
		}
		
		if( post.getParentId() != null ){
			Integer parentId = Integer.valueOf(Encrypt.decode(post.getParentId()));
			Cactus parent = cactusDao.getOne( parentId );
			cactus.setParentId(parent);
		}
		
		cactus = cactusDao.save(cactus);
		post.setId( Encrypt.encode( String.valueOf(cactus.getId()) ) );
		log.info(cactus.getCreateon());
		post.setCreateon( DateUtil.getDate( cactus.getCreateon()) );
		return post;
	}
	
	
	
	private List<PostBean> listToBean( List<Cactus> list ){
		List<PostBean> beans;
		if( list != null ){
			beans = new ArrayList<PostBean>();
		}else{
			return null;
		}
		for( Cactus c: list){
			PostBean bean = new PostBean();
			bean.setId( Encrypt.encode( String.valueOf(c.getId()) ) );
			bean.setCreateon( DateUtil.getDate(c.getCreateon()) );
			bean.setMessage( c.getMessage() );
			if( c.getImageUuid() != null){
				Document document = documentDao.load(c.getImageUuid());
				bean.setImageUuid( c.getImageUuid() );
				if( document != null ){
					bean.setDocument(document);
				}
			}
			beans.add( bean );
		}
		return beans;
	}
	

}
