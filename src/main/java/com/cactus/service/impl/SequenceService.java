package com.cactus.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cactus.constants.Constants;
import com.cactus.dao.interfaces.ISequenceDao;
import com.cactus.domain.Sequence;
import com.cactus.service.interfaces.ISequenceService;

@Service
public class SequenceService implements ISequenceService{
	
	@Autowired
	private ISequenceDao sequenceDao;
	
	@Transactional
	public int getSequence( String sequenceName ){
		int index = 1;
		Sequence sequence = sequenceDao.findBycode( sequenceName );
		if( sequence == null ){
			sequence = new Sequence();
			sequence.setCode( sequenceName );
			sequence.setSequenceNumber(index);
			sequence.setStatus( Constants.Table.ACTIVE );
		}else{
			index = sequence.getSequenceNumber();
			sequence.setSequenceNumber( ++index );
		}
		sequenceDao.save(sequence);
		return index;
	}
	


}
