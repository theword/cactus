package com.cactus.service.interfaces;

public interface ISequenceService {
	
	int getSequence( String sequenceName );
}
