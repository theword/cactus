package com.cactus.service.interfaces;

import java.util.Collection;

import com.cactus.domain.Employee;

public interface IEmployeeService {

	public Employee saveEmployee(Employee emp);
	public Boolean deleteEmployee(Long empId);
	public Employee editEmployee(Employee emp);
	public Employee findEmployee(Long empId);
	public Collection<Employee> getAllEmployees();
}
