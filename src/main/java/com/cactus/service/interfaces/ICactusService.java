package com.cactus.service.interfaces;

import java.util.List;

import com.cactus.bean.PostBean;

public interface ICactusService {

	void getAll();
	PostBean save(PostBean post );
	List<PostBean> getChunk( int pagging );
	boolean deletePost(PostBean post);
	List<PostBean> getPhotoDetailChunk( int pagging, String id );
	
}
