package com.cactus.constants;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
	public final static TimeZone TIME_ZONE= TimeZone.getTimeZone("UTC");
	public final static Locale LOCALE= new Locale("th","TH");
//	private final static Calendar CALENDAR = Calendar.getInstance( TIME_ZONE,  LOCALE);
	
	public static Date now(){
		return calendarNow().getTime();
	}
	
	public static Date getDate( long time ){
		Calendar cal = Calendar.getInstance( TIME_ZONE,  LOCALE);
		cal.setTimeInMillis(  time );
		return cal.getTime();
	}
	
	public static Calendar calendarNow(){
		Calendar cal = Calendar.getInstance( TIME_ZONE,  LOCALE);
		cal.setTime( new Date() );
		return cal;
	}
}
