package com.cactus.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/errorPage")
public class ErrorPage extends BaseController {

	@RequestMapping(value = {"404"}, method = RequestMethod.GET)
	public ModelAndView pageNotfound( Model model ) {
		model.addAttribute("errorMessage", "Page Not Found.");
		return new ModelAndView("../error_page/errorPage");
	}
	
	@RequestMapping(value = {"500"}, method = RequestMethod.GET)
	public ModelAndView errorEception( Model model ) {
		model.addAttribute("errorMessage", "Internal Server Error Exception");
		return new ModelAndView("../error_page/errorPage");
	}
	
	@RequestMapping(value = {"400"}, method = RequestMethod.GET)
	public ModelAndView unauthorizedException( Model model ) {
		model.addAttribute("errorMessage", "No permission to access this page");
		return new ModelAndView("../error_page/errorPage");
	}
}
