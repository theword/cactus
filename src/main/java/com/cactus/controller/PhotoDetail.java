package com.cactus.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cactus.bean.PostBean;
import com.cactus.service.interfaces.ICactusService;
import com.cactus.utils.Encrypt;


@RestController
@RequestMapping("/photoDetail")
public class PhotoDetail extends BaseController{
	
	private static final Logger log = Logger.getLogger(PhotoDetail.class);
	
	@Autowired
	private ICactusService cactusService;
	
	@RequestMapping(value = { "/detail/{photoId}", "/{photoId}" }, method = RequestMethod.GET)
	public void viewDetail( @PathVariable String photoId ){
		log.info("Encrypt photoId : " + photoId);
		log.info("Decrypt photoId : " + Encrypt.decode(photoId));
	}
	//"/detail/{pagging}/{photoId}", 
	@RequestMapping(value = {"/{pagging}/{photoId}"}, method = RequestMethod.GET )
	public @ResponseBody List<PostBean>  getChunk( @PathVariable(value="pagging")  int pagging, @PathVariable(value="photoId") String photoId){
		log.info( "pagging : " + pagging );
		log.info( "photoId : " + photoId );
		cactusService.getPhotoDetailChunk(pagging, photoId);
		return null;
	}
	
	@RequestMapping(value = {"/post"}, method = RequestMethod.POST )
	public @ResponseBody PostBean  postStatus( @RequestBody PostBean post ){
		log.info(post.toString());
		
		return cactusService.save(post);
	}
	
	
	
}
