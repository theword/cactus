package com.cactus.controller;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public abstract class BaseController implements /*extends ContextLoaderListener */
		ServletContextAware {

	private final Logger log = Logger.getLogger(BaseController.class);
	protected ServletContext servletContext;
	/*protected WebApplicationContext ctx;

	public <T> T getBean(Class<T> clazz) {
		this.log.info("Get Class bean" + clazz.toString());
		try {
			return getContext().getBean(clazz);
		} catch (Exception e) {
			this.log.error(e);
			return null;
		}
	}

	public Object getBean(String bean) {
		this.log.info("Get Class bean" + bean);
		try {
			return getContext().getBean(bean);
		} catch (Exception e) {
			this.log.error(e);
			return null;
		}
	}

	public <T> T getBean(String name, Class<T> clazz) {
		this.log.info("Get String bean " + name.toString());
		try {
			return getContext().getBean(name, clazz);
		} catch (Exception e) {
			this.log.error(e);
			return null;
		}
	}

	private WebApplicationContext getContext() {
		if (null == this.ctx)
			this.ctx = WebApplicationContextUtils
					.getWebApplicationContext(this.context);
		return this.ctx;
	}
*/
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public ServletContext getServletContext() {
		return this.servletContext;
	}

}
