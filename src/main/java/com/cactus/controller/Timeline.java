package com.cactus.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cactus.bean.PostBean;
import com.cactus.service.interfaces.ICactusService;


@Controller
@RequestMapping("/timeline")
public class Timeline extends BaseController{
	
	private static final Logger log = Logger.getLogger(Timeline.class);
	
	@Autowired
	private ICactusService cactusService;
	
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public ModelAndView viewTimeline(){
		return new ModelAndView("/timeline/index");
	}
	
	
	@RequestMapping(value = {"/getAll"}, method = RequestMethod.GET )
	public String getAll(){
		cactusService.getAll();
		return "redirect:/timeline/";
	}
	
	@RequestMapping(value = {"/delete"}, method = RequestMethod.POST )
	public @ResponseBody boolean deletePost( @RequestBody PostBean post){
		return cactusService.deletePost(post);
	}
	
	
	@RequestMapping(value = {"/getChunk"}, method = RequestMethod.POST )
	public @ResponseBody List<PostBean>  getChunk( @RequestBody int pagging ){
		log.info(	pagging );
		return cactusService.getChunk(pagging);
	}
	
	@RequestMapping(value = {"/post"}, method = RequestMethod.POST )
	public @ResponseBody PostBean  postStatus( @RequestBody PostBean share ){
		log.info(share.toString());
		return cactusService.save(share);
	}
	
	
/*	@RequestMapping(value = {"/getSequence/{sequenceName}"}, method = RequestMethod.GET )
	public @ResponseBody String  getSequence( @PathVariable("sequenceName")  String sequenceName ){
		log.info(sequenceName);

		return String.valueOf(sequenceService.getSequence(sequenceName));
	}*/
	
	
	
}
