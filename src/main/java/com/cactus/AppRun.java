package com.cactus;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@ComponentScan(basePackages={"com.cactus.configuration"})
public class AppRun {
	
    public static void main(String[] args) {
       SpringApplication.run( AppRun.class, args);
    }
    
   
}

