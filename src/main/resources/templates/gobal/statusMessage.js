

(function(){
	'use strict';
	angular.module('timeline',[]);
	
	angular.module('timeline')
	.service('SharedStatus',[ '$http', function( $http ){
		return{
			post : function( data ){
				return $http.post( '/timeline/post', data );
			},
			postProgress :  function( data ){
				return $http.post( '/photoDetail/post', data );
			}
		};
	}]);
	
  	angular.module('timeline')
  	.service('Timeline',['$http',function($http){
  		var timeline = [];
  		var pagging = 0;
  		
		this.list =function(){
			return timeline;
		};
		this.set = function( data ){
			timeline = data;
		};
		this.add = function( data ){
			timeline.unshift(data);
		};
		this.deletePost = function( param ){
			 return $http.post("/timeline/delete", param ).success(function(data){
  				 if( data == true ){
  					for( var i=0; i<timeline.length; i++ ){
  						if( timeline[i]['id'] == param['id'] ){
  							timeline.splice(i, 1);
  							break;
  							console.log(param['id'] );
  						}
  					}
  				 }
  				 
  			}).error(function( data ){
  				console.log(data);
  			});
		};
  		this.pagging = function(){
  			 return $http.post("/timeline/getChunk", pagging ).success(function(data){
  				 console.log(data);
  				 if( data.length > 0 ){
  					timeline = [].concat(timeline,data);
  					pagging++;
  				 }
  			}).error(function( data ){
  				
  			});
  		};
  		
  		this.getPagging = function(){
  			return pagging;
  		};
  	}]);

	/*.service('bindToCtrl', function($parse) {
	  return function bindToCtrl(scope, ctrl, expr, prop) {
	    // modified code borrowed from $compile
	    var getter = $parse(expr);
	    var setter = getter.assign;
	    var lastValue = ctrl[prop] = getter(scope.$parent);
	    scope.$watch(function() {
	      var parentValue = getter(scope.$parent);
	      if (parentValue !== ctrl[prop]) {
	        // we are out of sync and need to copy
	        if (parentValue !== lastValue) {
	          // parent changed and it has precedence
	          ctrl[prop] = parentValue;
	        } else {
	          setter(scope.$parent, parentValue = ctrl[prop]);
	        }
	      }
	      lastValue = parentValue;
	      return lastValue;
	    }, null);
	  };*/
}());

/*


*/