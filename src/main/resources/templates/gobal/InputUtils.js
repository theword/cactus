(function(angular) {
			  'use strict';
	angular.module('input.utils',['ui.bootstrap']);

	angular.module('input.utils')
	.directive('formStatus',[ '$compile', '$timeout', 'SharedStatus', 'Timeline',
	                          function( $compile, $timeout, SharedStatus, Timeline ){
		var option = {
				id : Math.random().toString(36).slice(2)
				,acceptType: 'image/*'
					,input: '<input id="{{id}}" name="{{id}}" type="file" accept="{{accepType}}" ng-disabled="false">'	
		};

		function link (scope, element, attrs, ctrl, transclude) {
			scope['status'] = {};
			angular.extend(scope, option);
			

			element.find('input').bind('change', function(even){
			
				even.preventDefault();
				even.stopPropagation();
				scope.$apply( function (scope){
					scope.setFiles(even.target);
					scope.uploadFile();
				});
			});


			//============== DRAG & DROP =============
			// source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/

			scope.setFiles = function(element) {
//				scope.$apply(function(scope) {
				console.log('files:', element.files);
				// Turn the FileList object into an Array
				scope.files = []
				for (var i = 0; i < element.files.length; i++) {
					scope.files.push(element.files[i])
				}
				scope.progressVisible = false
//				});
			};

			scope.uploadFile = function() {
				var fd = new FormData()
				for (var i in scope.files) {
					fd.append("file", scope.files[i])
				}
				var xhr = new XMLHttpRequest()
				xhr.upload.addEventListener("progress", uploadProgress, false)
				xhr.addEventListener("load", uploadComplete, false)
				xhr.addEventListener("error", uploadFailed, false)
				xhr.addEventListener("abort", uploadCanceled, false)
				xhr.open("POST", "/archive/upload")
				scope.progressVisible = true
				xhr.send(fd)
			}

			scope.post = function(){
				scope.onPost();
				/*
				SharedStatus.post(scope.status).success(function(data){
					Timeline.add(data);
					scope.status = {};
				}).error(function(error){

				});*/
			};

			function uploadProgress(evt) {
				scope.$apply(function(){
					if (evt.lengthComputable) {
						scope.progress = Math.round(evt.loaded * 100 / evt.total)
					} else {
						scope.progress = 'unable to compute'
					}
				})
			}

			function uploadComplete(evt) {
				/* This event is raised when the server send back a response */
				scope.$apply(function(  ){
					var data = JSON.parse(evt.target.responseText);
					scope['status']['imageUuid'] = data['uuid'];
				});
			}

			function uploadFailed(evt) {
				alert("There was an error attempting to upload the file.")
			}

			function uploadCanceled(evt) {
				scope.$apply(function(){
					scope.progressVisible = false
				})
				alert("The upload has been canceled by the user or the browser dropped the connection.")
			}
		};


		return {
			templateUrl: '/gobal/template/statusMessage.html',
			restrict: 'E',
			transclude: true,
			replace:true,
			scope: {status: '=ngModel', onPost: '&'},
			link: link
		}
	}]);

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/************* SERVICE ***********/
	angular.module('input.utils')
	.service('PopupService', ['$uibModal', function($uibModal){

		return function ( options ){
			return $uibModal.open({
				size: 'md',
				backdrop:'static',
				animation: true,
				template: /*'<div class="modal-header panel-default">\
					            <h4 class="modal-title">{{vm.options.title}}</h4>\
				             </div>\
					         <div class="modal-body">{{vm.options.content}}</div>\
					         <div class="modal-footer">\
					                <button class="btn btn-primary" type="button" ng-click="vm.confirm()">{{vm.options.confirmBtn}}</button>\
					                <button class="btn btn-warning" type="button" ng-click="vm.cancel()">{{vm.options.cancelBtn}}</button>\
					         </div>',*/
					'<div class="dialog-modal" style="color:black;" > \
						<div class="modal-content ">\
							<div class="panel-default" ><h5 class="modal-title panel-heading">{{vm.options.title}}</h5>\</div>\
							<div class="modal-body text-center" style="padding:20px 0">{{vm.options.content}}</div>\
							<div class="modal-footer" > \
								<button class="btn btn-primary btn-sm" ng-click="vm.confirm()">{{vm.options.confirmBtn}}</button> \
								<button class="btn btn-default btn-sm" ng-click="vm.cancel()" >{{vm.options.cancelBtn}}</button> \
							</div> \
						</div>\
					</div>',
					controller: function ( $uibModalInstance ){
						var self = this;
						self.options = options;
						self.confirm = function () {
							$uibModalInstance.close(true);
						};

						self.cancel = function () {
							$uibModalInstance.dismiss(false);
						};
					},
					controllerAs: 'vm'

			});
		}
	}]);


	})(window.angular);