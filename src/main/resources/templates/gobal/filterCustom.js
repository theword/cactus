(function(){
	'use strict';
	angular.module('fillter.custom',[]);
	
	angular.module('fillter.custom')
	.filter('dateStatus',['$filter', function($filter) {    
	    var angularDateFilter = $filter('date');
	    return function(date) {
	       return angularDateFilter(date, 'MMMM dd \'at\' HH:mma');
	    }
	}]);

}());