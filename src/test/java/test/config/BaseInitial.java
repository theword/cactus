package test.config;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cactus.AppRun;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AppRun.class)
@WebIntegrationTest
public abstract class BaseInitial {

}
