package test.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cactus.bean.PostBean;
import com.cactus.service.interfaces.ICactusService;
import com.cactus.utils.Encrypt;

import test.config.BaseInitial;

public class test extends BaseInitial{
	
	private @Autowired ICactusService cactusService;
	
	@Test
	public void getPhotoDetail(){
		System.out.println( "--------------" );
		List<PostBean> result = cactusService.getPhotoDetailChunk(0, "OYELRU3sQm8=");
		for(PostBean p : result ){
			System.out.println( Encrypt.decode(p.getId()) );
		}
		System.out.println( "--------------" );
	}

}
